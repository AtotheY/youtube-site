import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <a
          className="App-link"
          href="https://www.youtube.com/playlist?list=PLQg6GaokU5CyvExiaMgXP_BS5WWNBfZJN"
          target="_blank"
          rel="noopener noreferrer"
        >
          A Youtube series to learn Reactjs :)
        </a>
        <img src={logo} className="App-logo" alt="logo" />
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/OH4msPNM2CI"
          frameborder="0"
          title="Learn React Youtube Series"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        />
      </header>
    </div>
  );
}

export default App;
